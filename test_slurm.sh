#!/bin/bash
#SBATCH --nodes=1
#SBATCH --job-name=test_slurm
#SBATCH --output=./test_slurm_out.txt
#SBATCH --ntasks=1
#SBATCH --time=60:00
#SBATCH --mem-per-cpu=1G

# source my bash_profile to give details of the environment
source /home/avajpeyi/.bash_profile
# command to run
python /fred/oz006/avajpeyi/slurm_testing/test.py

# to run, $sbatch test_slurm.sh
