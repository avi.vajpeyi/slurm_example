# SLURM submission example

To submit this example job:
`sbatch test_slurm.sh`

## Testing code
use `sinteractive`
(need to use `ssh -Y ozstar` to be able to use sinteractive)


Some other options for sbatch
```
#sbatch --cpus-per-task=1
#sbatch -time=48:00:00 
#sbatch -mem=2000mb
#sbatch -gres=gpu=1  // 1 gpu
        -gres=gpu=gpu:p100:2 //2 p100 gpus
```